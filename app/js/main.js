/* global FileReader, Image, localStorage */
const vector = require('vector-do')

const pixelsToInches = x => vector.divide(x, ppi)
const inchesToPixels = x => vector.multiply(x, ppi)

const photoSelector = document.getElementById('photo')
const saveButton = document.getElementById('save')
const loadButton = document.getElementById('load')
const debugContainer = document.getElementById('debugContainer')

const canvas = document.querySelector('canvas')
const context = canvas.getContext('2d')
const canvasSize = [canvas.width, canvas.height]
const ppi = canvasSize[0] / 15

const photo = {
  position: [0, 0],
  size: [0, 0]
}
const ui = {
  dragging: false,
  clickPosition: [0, 0],
  photoClickPosition: [0, 0]
}

function log (msg) {
  debugContainer.innerHTML = '<pre>' + msg + '</pre>'
}

const is = x => x
const isNotPositive = x => x <= 0

const isSurfaceFull = (position, size) => {
  if (position.every(isNotPositive)) {
    const pixelPosition = inchesToPixels(position)
    const pixelSize = inchesToPixels(size)

    const bottomRight = vector.add(pixelPosition, pixelSize)
    const full = vector.do(bottomRight, canvasSize, (a, b) => a >= b)

    return full.every(is)
  } else {
    return false
  }
}

const getEventPosition = event => pixelsToInches([event.pageX, event.pageY])

canvas.addEventListener('mousedown', event => {
  ui.dragging = true
  ui.clickPosition = getEventPosition(event)
  ui.photoClickPosition = photo.position
})

canvas.addEventListener('mousemove', event => {
  if (ui.dragging) {
    const cursorPosition = getEventPosition(event)
    const delta = vector.subtract(cursorPosition, ui.clickPosition)
    const newPhotoPosition = vector.add(ui.photoClickPosition, delta)

    if (isSurfaceFull(newPhotoPosition, photo.size)) {
      photo.position = newPhotoPosition
    } else {
      const baseX = [ui.photoClickPosition[0], photo.position[1]]
      const deltaX = [delta[0], delta]
      const newPhotoPositionX = vector.add(baseX, deltaX)
      if (isSurfaceFull(newPhotoPositionX, photo.size)) {
        photo.position = newPhotoPositionX
      }

      const baseY = [photo.position[0], ui.photoClickPosition[1]]
      const deltaY = [0, delta[1]]
      const newPhotoPositionY = vector.add(baseY, deltaY)
      if (isSurfaceFull(newPhotoPositionY, photo.size)) {
        photo.position = newPhotoPositionY
      }
    }
  }
})

const releaseDrag = event => (ui.dragging = false)
canvas.addEventListener('mouseup', releaseDrag)
canvas.addEventListener('mouseleave', releaseDrag)

canvas.addEventListener('wheel', event => {
  const changeInZoom = -1 * event.deltaY / 10000
  const newSize = vector.multiply(photo.size, 1 + changeInZoom)
  const changeInSize = vector.subtract(newSize, photo.size)
  const offset = vector.divide(changeInSize, 2)
  const newPosition = vector.subtract(photo.position, offset)

  if (isSurfaceFull(newPosition, newSize)) {
    photo.position = newPosition
    photo.size = newSize
  }
})

const render = () => {
  window.requestAnimationFrame(render)

  if (ui.image) {
    context.clearRect(0, 0, ...canvasSize)

    const pixelPosition = inchesToPixels(photo.position)
    const pixelSize = inchesToPixels(photo.size)

    context.drawImage(ui.image, ...pixelPosition, ...pixelSize)

    log(JSON.stringify(photo, null, 2))
  }
}

photoSelector.onchange = event => {
  const file = event.target.files[0]

  if (file.type.startsWith('image/')) {
    const reader = new FileReader()

    reader.onload = event => {
      const image = new Image()
      image.src = reader.result

      image.onload = () => {
        ui.image = image

        const imageRatio = image.naturalWidth / image.naturalHeight
        const canvasRatio = canvasSize[0] / canvasSize[1]

        if (canvasRatio > imageRatio) {
          const scale = 15 / image.naturalWidth
          photo.size = [15, image.naturalHeight * scale]
        } else {
          const scale = 10 / image.naturalHeight
          photo.size = [image.naturalWidth * scale, 10]
        }

        render()
      }
    }

    reader.readAsDataURL(file)
  }
}

saveButton.addEventListener('click', () => {
  localStorage.setItem('description', JSON.stringify(photo))
})

loadButton.addEventListener('click', () => {
  const description = JSON.parse(localStorage.getItem('description'))
  photo.size = description.size
  photo.position = description.position
})

log('Test application ready')
